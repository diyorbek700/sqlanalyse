WITH SalesByYear AS (
    SELECT
        prod_subcategory,
        EXTRACT(YEAR FROM prod_eff_from) AS sale_year,
        SUM(prod_list_price) AS total_sales
    FROM
        sh.products
    WHERE
        EXTRACT(YEAR FROM prod_eff_from) BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory, sale_year
),
SalesComparison AS (
    SELECT
        prod_subcategory,
        sale_year,
        total_sales,
        LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY sale_year) AS previous_year_sales
    FROM
        SalesByYear
),
HigherSalesSubcategories AS (
    SELECT DISTINCT
        prod_subcategory
    FROM
        SalesComparison
    WHERE
        total_sales > COALESCE(previous_year_sales, 0)
    ORDER BY
        prod_subcategory
)
SELECT
    prod_subcategory
FROM
    HigherSalesSubcategories;
